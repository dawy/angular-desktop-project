import { NgModule } from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: 'app/home/home.module#HomeModule'
  },
  {
    path: 'desktop',
    loadChildren: 'app/desktop/desktop.module#DesktopModule'
  },
  {
    path: 'search',
    loadChildren: 'app/search/search.module#SearchModule'
  },
  {
    path: 'fake',
    loadChildren: 'app/fake/fake.module#FakeModule'
  },
  {
    path: 'fake-video',
    loadChildren: 'app/fakeVideo/fakeVideo.module#FakeVideoModule'
  },
  {
    path: 'powerpoint',
    loadChildren: 'app/powerpoint/powerpoint.module#PowerpointModule'
  },
  {
    path: 'mail/:delay',
    loadChildren: 'app/mail/mail.module#MailModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
