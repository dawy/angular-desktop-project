import {RouterModule, Routes} from '@angular/router';
import {SearchComponent} from './search.component';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: SearchComponent
  },
  {
    path: 'result',
    loadChildren: 'app/search/result/result.module#ResultModule'
  }
];
@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule { }
