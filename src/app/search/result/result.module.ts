import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ResultComponent} from './result.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ResultComponent
  }
];

@NgModule({
  declarations: [ResultComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class ResultModule { }
