import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {SettingsService} from '../../core/services';
import {Result} from '../../Model';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {
  results: Array<Result> = [];
  showImage = false;
  imgSrc = '';
  constructor(private router: Router, public settingsService: SettingsService, public domSanitizer: DomSanitizer) { }

  ngOnInit() {
    this.results = this.settingsService.results;
  }
  goHome() {
    this.router.navigateByUrl('');
  }
  goToFake() {
    this.router.navigateByUrl('fake');
  }
  goToFakeVideo() {
    this.router.navigateByUrl('fake-video');
  }
  imgClick(event) {
    this.imgSrc = event.target.src;
    this.showImage = true;
  }

}
