import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SearchComponent} from './search.component';
import { ResultComponent } from './result/result.component';
import {SearchRoutingModule} from './search-routing.module';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [SearchComponent],
  imports: [
    CommonModule,
    SearchRoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class SearchModule { }
