export class Mail {
  author: string;
  title: string;
  message: string;
  read: boolean;
  images: Array<{path: string, name: string, checked: boolean}>;

  constructor(data?: any) {
    this.images = [];
    if (data !== undefined) {
      this.author = data.author;
      this.title = data.title;
      this.message = data.message;
      this.read = data.read;
      this.images = data.images;
    } else {
      this.author = 'Borchert Borchert';
      this.title = 'Some Title';
      this.message = 'too lazy to search for lorem ipsum so aijshkjagkjlaskjlhsfjlhsdfljfaljalashgkjlasgjlsabgasjkgblsakjghklasjghalskjg';
      this.read = false;
    }
  }
}
