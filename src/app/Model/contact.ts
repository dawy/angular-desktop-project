export class Contact {
  name: string;

  constructor(data?: {name: string}) {
    if (data !== undefined) {
      this.name = data.name;
    } else {
      this.name = 'Bochert Borchertovic';
    }
  }
}
