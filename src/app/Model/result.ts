export class Result {
  header: string;
  link: string;
  content: string;

  constructor(data?: any) {
    if (data !== undefined) {
      this.header = data.header;
      this.link = data.link;
      this.content = data.content;
    } else {
      this.header = 'Dies ist eine Ergebnisüberschrift, die interessant sein sollte';
      this.link = 'https://www.result-link.de/';
      this.content = 'Dies ist ein Ergebnisinhalt, er sollte etwas länger als der Header sein';
    }
  }
}
