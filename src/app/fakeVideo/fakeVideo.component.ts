import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {SettingsService} from '../core/services';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-fake',
  templateUrl: './fakeVideo.component.html',
  styleUrls: ['./fakeVideo.component.scss']
})
export class FakeVideoComponent implements OnInit {
  data: {header: string, text: string, image: string, video: string};
  constructor(private router: Router, private settingsService: SettingsService, public domSanitizer: DomSanitizer) { }
  ngOnInit() {
    this.settingsService.loadFakeVideoPageContent();
    this.data = this.settingsService.fakeVideoPageContent;
  }
  goHome() {
    this.router.navigateByUrl('/search/result');
  }
}
