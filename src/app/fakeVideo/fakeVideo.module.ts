import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {FakeVideoComponent} from './fakeVideo.component';

const routes: Routes = [
  {
    path: '',
    component: FakeVideoComponent
  }
  ];

@NgModule({
  declarations: [FakeVideoComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class FakeVideoModule { }
