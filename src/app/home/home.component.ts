import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MailServiceService, SettingsService} from '../core/services';
import {Mail} from '../Model/mail';
import {Contact} from '../Model';



export enum Picker {
  Mail,
  Result,
  Fake
}
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  result;
  authorName = '';
  message = '';
  read = false;
  title = '';
  resultHeader = '';
  resultLink = '';
  resultContent = '';
  fakeHeader = '';
  fakeText = '';
  fakeImage: any;
  sendMailDelay: number;
  contactName = '';
  picker = Picker;
  currentStep;
  deleteAtPos = 0;
  mailImages: Array<{path: string, name: string}> = [];
  threeImages: {firstPath: string, secondPath: string, thirdPath: string} = {firstPath: '', secondPath: '', thirdPath: ''};
  fakeVideo: {header: string, text: string, image: string, video: string} = {header: '', text: '', image: '', video: ''};
  constructor(private router: Router, private mailService: MailServiceService, private settingsService: SettingsService) { }

  ngOnInit() {
  }
  imageChanged(event) {
    console.log(event.target.value);
    const img = {
      path: 'file:' + event.target.files[0].path,
      name: '' + event.target.value,
      checked: false
    };
    try {
      this.mailImages.push(img);
    } catch (e) {
      this.mailImages = [img];
    }
    console.log(this.mailImages);
  }
  redirect() {
    this.mailService.loadMails();
    this.router.navigateByUrl('/mail/' + 0);
  }
  search() {
    this.router.navigateByUrl('/search');
  }
  powerpoint() {
    this.router.navigateByUrl('/powerpoint');
  }
  desktop() {
    this.router.navigateByUrl('/desktop');
  }
  addContact() {
    this.settingsService.addContact(new Contact({name: this.contactName}));
  }
  clearContacts() {
    this.settingsService.clearContacts();
  }
  addEmail() {
    const data = {
      author: this.authorName,
      title: this.title,
      read: this.read,
      message: this.message,
      images: this.mailImages
    };
    this.mailService.addMail(data);
    this.mailImages = [];
  }
  addEmailWithDelay() {
    const data = {
      author: this.authorName,
      title: this.title,
      read: this.read,
      message: this.message,
      images: this.mailImages
    };
    const mail = new Mail(data);
    this.mailService.setMailToAddWithDelay(mail);
    this.router.navigateByUrl('/mail/' + this.sendMailDelay);
  }
  clearMails() {
    this.mailService.clearEmails();
  }
  addResult() {
    const data = {
      header: this.resultHeader,
      link: this.resultLink,
      content: this.resultContent
    };
    console.log('home data' + data);
    this.settingsService.addResult(data);
  }
  setFakeVideo() {
    this.settingsService.setFakeVideoPageContent(this.fakeVideo);
  }
  resetFakeVideo() {
    this.settingsService.clearFakeVideoPageContent();
  }
  setFake() {
    const data = {
      header: this.fakeHeader,
      text: this.fakeText,
      image: this.fakeImage
    };
    this.settingsService.setFakePageContent(data);
  }
  resetFake() {
    this.settingsService.clearFakePageContent();
  }
  test() {
    this.mailService.loadMails();
  }
  onFakeImage(event) {
    this.fakeImage = 'file:' + event.target.files[0].path;
  }
  onFakeVideoImage(event) {
    this.fakeVideo.image = 'file:' + event.target.files[0].path;
  }
  onFakeVideo(event) {
    this.fakeVideo.video = 'file:' + event.target.files[0].path;
  }
  imageOneChanged(event) {
    this.threeImages.firstPath = 'file:' + event.target.files[0].path;
    this.settingsService.addResultImages(this.threeImages);
    console.log(this.threeImages);
  }
  imageTwoChanged(event) {
    this.threeImages.secondPath = 'file:' + event.target.files[0].path;
    this.settingsService.addResultImages(this.threeImages);
    console.log(this.threeImages);
  }
  imageThreeChanged(event) {
    this.threeImages.thirdPath = 'file:' + event.target.files[0].path;
    this.settingsService.addResultImages(this.threeImages);
    console.log(this.threeImages);
  }
}
