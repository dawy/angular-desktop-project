import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MailComponent } from './mail.component';

const routes: Routes = [
  {
    path: '',
    component: MailComponent
  },
  {
    path: 'read',
    loadChildren: 'app/mail/read/read.module#ReadModule'
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MailRoutingModule {}
