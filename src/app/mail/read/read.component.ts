import { Component, OnInit } from '@angular/core';
import {MailServiceService, SettingsService} from '../../core/services';
import {Mail} from '../../Model/mail';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.scss']
})
export class ReadComponent implements OnInit {
  mail: Mail;
  mailImages: Array<{path: string}> = new Array<{path: string}>();
  bigImage: boolean;
  constructor(private mailService: MailServiceService, private settings: SettingsService, private domSanitizer: DomSanitizer) { }

  ngOnInit() {
    this.mail = this.mailService.getMailToRead();
    this.mailImages = this.settings.getMailImages();
    console.log(this.mail);
    console.log(this.mailImages);
  }
}
