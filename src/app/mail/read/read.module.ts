import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ReadComponent} from './read.component';
import {SharedModule} from '../../shared/shared.module';
import {CoreModule} from '../../core/core.module';

const routes: Routes = [
  {
    path: '',
    component: ReadComponent
  }
];

@NgModule({
  declarations: [ReadComponent],
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,
    RouterModule.forChild(routes)
  ],
  exports: [ReadComponent]
})
export class ReadModule { }
