import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MailRoutingModule } from './mail-routing.module';

import { MailComponent } from './mail.component';
import { SharedModule } from '../shared/shared.module';
import { ReadComponent } from './read/read.component';
import {ReadModule} from './read/read.module';

@NgModule({
  declarations: [MailComponent],
  imports: [CommonModule, SharedModule, MailRoutingModule, ReadModule]
})
export class MailModule {}
