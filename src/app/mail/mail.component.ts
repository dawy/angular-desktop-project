import { Component, OnInit } from '@angular/core';
import {MailServiceService, SettingsService} from '../core/services';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-mail',
  templateUrl: './mail.component.html',
  styleUrls: ['./mail.component.scss']
})
export class MailComponent implements OnInit {
  delay: number;
  constructor(private settings: SettingsService, private mailService: MailServiceService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.mailService.showNewMailNumber = false;
    this.settings.loadContacts();
    const that = this;
    try {
      this.delay = parseInt(this.activatedRoute.snapshot.paramMap.get('delay'), 10);
      if (this.delay !== 0) {
        console.log(this.delay);
        setTimeout(function () {
          that.mailService.addMailWithDelay(that.mailService.getMailToAddWithDelay());
        }, this.delay * 1000);
      }
    } catch (e) {
    }
  }

}
