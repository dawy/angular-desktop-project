import { Injectable } from '@angular/core';
import {Mail} from '../../../Model/mail';


@Injectable({
  providedIn: 'root'
})
export class MailServiceService {
  mailList: Array<Mail> = [];
  mailsToDelete: Array<Mail> = [];
  testMailList: Array<Mail> = [];
  private mailToRead: Mail;
  showNewMailNumber = false;
  mailToAddWithDelay: Mail;
  storage = window.localStorage;
  constructor() {
  }

  clearEmails () {
    this.mailList = [];
    this.storage.setItem('mails', JSON.stringify(this.mailList));
  }
  addMail(data) {
    console.log(new Mail(data));
    this.mailList.push(new Mail(data));
    console.log(this.mailList);
    this.storage.setItem('mails', JSON.stringify(this.mailList));
    this.loadMails();
  }
  addMailWithDelay(mail: Mail) {
    console.log(mail);
    this.mailList.unshift(mail);
    console.log(this.mailList);
    this.storage.setItem('mails', JSON.stringify(this.mailList));
    const snd = new Audio('assets/sound/beep.mp3');
    snd.play();
    this.loadMails();
    this.showNewMailNumber = true;
  }
  setMailToAddWithDelay(mail: Mail) {
    this.mailToAddWithDelay = mail;
  }
  getMailToAddWithDelay() {
    return this.mailToAddWithDelay;
  }
  addMailToDelete(mail: Mail) {
    this.mailsToDelete.push(mail);
    console.log(this.mailsToDelete);
  }
  removeMailToDelete(mail: Mail) {
    this.mailsToDelete.forEach((item, index) => {
      if (item === mail) {
         this.mailsToDelete.splice(index, 1);
      }
    });
    console.log(this.mailsToDelete);
  }
  deleteMailsToDelete() {
    if (this.mailList.length > 0 && this.mailsToDelete.length > 0) {
      this.mailList.forEach((item, index) => {
        this.mailsToDelete.forEach((ite, ind) => {
          if (item === ite) {
            this.mailList.splice(index, 1);
            this.mailsToDelete.splice(ind, 1);
            this.deleteMailsToDelete();
          }
        });
      });
    } else {
      this.storage.setItem('mails', JSON.stringify(this.mailList));
    }
  }
  loadMails() {
    try {
      this.mailList = JSON.parse(this.storage.getItem('mails'));
      console.log('gotcha');
    } catch (e) {
      console.log('nogotcha');
      this.mailList = [];
    }
  }
  setMailToRead(mail) {
    this.mailToRead = mail;
  }
  clearMailToRead() {
    this.mailToRead = null;
  }
  getMailToRead() {
    return this.mailToRead;
  }
}
