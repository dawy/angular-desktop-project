import { Injectable } from '@angular/core';
import {Contact, Result} from '../../../Model';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';


@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  results: Array<Result>;
  contactList: Array<Contact>;
  storage = window.localStorage;
  searchText: string;
  powerPointImage = {};
  fakePageContent: {header: string, text: string, image: string};
  fakeVideoPageContent: {header: string, text: string, image: string, video: string};
  constructor() {
    this.contactList = new Array<Contact>();
    this.loadResults();
    this.loadFakeVideoPageContent();
    this.loadFakePageContent();
  }
  addContact(contact) {
    console.log(contact);
    this.contactList.push(new Contact(contact));
    console.log(this.contactList);
    try {
      this.storage.setItem('mailContacts', JSON.stringify(this.contactList));
    } catch (e) {
      console.log('NoGotcha');
    }

  }
  loadContacts() {
    try {
      this.contactList = JSON.parse(this.storage.getItem('mailContacts'));
    } catch (e) {
      this.contactList = [];
    }
  }
  getContacts() {
    return this.contactList;
  }
  clearContacts() {
    this.contactList = [];
    try {
      this.storage.removeItem('mailContacts');
    } catch (e) {
      console.log(e);
    }
  }
  setPowerPointImage(img: {src: string}) {
    this.powerPointImage = img;
  }
  addResult(data) {
    this.results.push(new Result(data));
    this.storage.setItem('resultsData', JSON.stringify(this.results));
  }
  setFakeVideoPageContent(data) {
    this.fakeVideoPageContent = {
      header: data.header,
      text: data.text,
      image: data.image,
      video: data.video
    };
    this.storage.setItem('fakeVideoPage', JSON.stringify(this.fakeVideoPageContent));
    this.loadFakeVideoPageContent();
  }
  loadFakeVideoPageContent() {
    try {
      this.fakeVideoPageContent = JSON.parse(this.storage.getItem('fakeVideoPage'));
    } catch (e) {
      this.fakeVideoPageContent = {header: '', text: '', image: '', video: ''};
    }
  }
  loadResults() {
    try {
      this.results = JSON.parse(this.storage.getItem('resultsData'));
    } catch (e) {
      this.results = new Array<Result>();
    }
  }
  clearFakeVideoPageContent() {
    this.fakeVideoPageContent = {header: '', text: '', image: '', video: ''};
    this.storage.setItem('fakeVideoPage', JSON.stringify(this.fakeVideoPageContent));
  }
  setFakePageContent(data) {
    this.fakePageContent = {
      header: data.header,
      text: data.text,
      image: data.image
    };
    this.storage.setItem('fakePage', JSON.stringify(this.fakePageContent));
    this.loadFakePageContent();
  }
  loadFakePageContent() {
    try {
      this.fakePageContent = JSON.parse(this.storage.getItem('fakePage'));
    } catch (e) {
      this.fakePageContent = {header: '', text: '', image: ''};
    }
  }
  clearFakePageContent() {
    this.fakePageContent = {header: '', text: '', image: ''};
    this.storage.setItem('fakePage', JSON.stringify(this.fakePageContent));
  }
  clearResults() {
    this.results = [];
    this.storage.setItem('resultsData', JSON.stringify(this.results));
  }
  clearResult(pos: number) {
    this.results.splice(pos, 1);
  }
  addResultImages(data: {firstPath: string, secondPath: string, thirdPath: string}) {
    this.storage.setItem('resultImages', JSON.stringify(data));
  }
  addMailImages(data: Array<{path: string}>) {
    console.log(data);
    this.storage.setItem('mailImages', JSON.stringify(data));
  }
  getMailImages() {
    let images: Array<{path: string}> = new Array<{path: string}>();
    try {
      images = JSON.parse(this.storage.getItem('mailImages'));
    } catch (e) {
      images = [];
    }
    console.log(images);
    return images;
  }
  getResultImages() {
    console.log(JSON.parse(this.storage.getItem('resultImages')));
    return JSON.parse(this.storage.getItem('resultImages'));
  }
  removeResultImages() {
    this.storage.setItem('resultImages', JSON.stringify([]));
  }
}
