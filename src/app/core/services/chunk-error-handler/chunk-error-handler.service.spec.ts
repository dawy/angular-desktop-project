import { TestBed } from '@angular/core/testing';

import { ChunkErrorHandlerService } from './chunk-error-handler.service';

describe('ChunkErrorHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChunkErrorHandlerService = TestBed.get(ChunkErrorHandlerService);
    expect(service).toBeTruthy();
  });
});
