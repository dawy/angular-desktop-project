import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PowerpointComponent } from './powerpoint.component';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';

const routes = [
  {
    path: '',
    component: PowerpointComponent
  }
  ];

@NgModule({
  declarations: [PowerpointComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class PowerpointModule { }
