import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FakeComponent } from './fake.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: FakeComponent
  }
  ];

@NgModule({
  declarations: [FakeComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class FakeModule { }
