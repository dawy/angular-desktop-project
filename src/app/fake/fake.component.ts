import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {SettingsService} from '../core/services';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-fake',
  templateUrl: './fake.component.html',
  styleUrls: ['./fake.component.scss']
})
export class FakeComponent implements OnInit {
  data: {header: string, text: string, image: string};
  constructor(private router: Router, private settingsService: SettingsService, public domSanitizer: DomSanitizer) { }
  ngOnInit() {
    this.settingsService.loadFakePageContent();
    this.data = this.settingsService.fakePageContent;
  }
  goHome() {
    this.router.navigateByUrl('/search/result');
  }
}
