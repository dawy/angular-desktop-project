import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

import {MailHeaderComponent,
  MailListComponent,
  MailMenuComponent,
  PageNotFoundComponent,
  HeaderIconComponent,
  HeaderSearchComponent,
  ContactListComponent,
  MenuItemsComponent,
  ContactComponent,
  MenuItemComponent,
  MailItemComponent,
  BarComponent,
  ResultsComponent,
  LeftBarComponent,
  ContentComponent,
  WaveformComponent,
  HeaderComponent} from './components/';
import { WebviewDirective } from './directives/';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [PageNotFoundComponent,
    WebviewDirective,
    MailMenuComponent,
    MailListComponent,
    MailHeaderComponent,
    HeaderIconComponent,
    HeaderSearchComponent,
    ContactListComponent,
    MenuItemsComponent,
    ContactComponent,
    MenuItemComponent,
    MailItemComponent,
    BarComponent,
    ResultsComponent,
    LeftBarComponent,
    ContentComponent,
    WaveformComponent,
    HeaderComponent],
  imports: [CommonModule, TranslateModule, FormsModule],
  exports: [TranslateModule,
    WebviewDirective,
    MailHeaderComponent,
    MailMenuComponent,
    MailListComponent,
    HeaderSearchComponent,
    BarComponent,
    WaveformComponent,
    ResultsComponent, HeaderComponent, LeftBarComponent, ContentComponent, ContactListComponent]
})
export class SharedModule {}
