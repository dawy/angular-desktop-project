import {Component, Input, OnInit} from '@angular/core';
import {MailServiceService} from '../../../../../core/services';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})

export class MenuItemComponent implements OnInit {
  @Input() label = 'Posteingang';
  @Input() iconPath = 'inbox-icon.svg';
  @Input() highlight = false;
  constructor(public mailService: MailServiceService) { }

  ngOnInit() {
  }

}
