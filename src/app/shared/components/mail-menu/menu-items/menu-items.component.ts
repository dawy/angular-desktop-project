import { Component, OnInit } from '@angular/core';
import {MailServiceService} from '../../../../core/services';

@Component({
  selector: 'app-menu-items',
  templateUrl: './menu-items.component.html',
  styleUrls: ['./menu-items.component.scss']
})
export class MenuItemsComponent implements OnInit {

  constructor(private mailService: MailServiceService) { }

  ngOnInit() {
  }

}
