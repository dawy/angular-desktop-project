import { Component, OnInit } from '@angular/core';
import {SettingsService} from '../../../../core/services';
import {Contact} from '../../../../Model';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {

  contacts: Array<Contact> = new Array<Contact>();
  constructor(private settings: SettingsService) { }

  ngOnInit() {
     try {
       this.settings.loadContacts();
       this.contacts = this.settings.contactList;
     } catch (e) {
       this.contacts = [];
     }
     console.log(this.contacts);
  }

}
