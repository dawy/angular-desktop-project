import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MailServiceService, SettingsService} from '../../../core/services';
import {Mail} from '../../../Model/mail';

@Component({
  selector: 'app-mail-list',
  templateUrl: './mail-list.component.html',
  styleUrls: ['./mail-list.component.scss']
})
export class MailListComponent implements OnInit {
  mails: Array<Mail> = [];
  mailImages: Array<{path: string}> = [];
  constructor(private router: Router, private mailService: MailServiceService) { }

  ngOnInit() {
    try {
      this.mailService.loadMails();
      this.mails = this.mailService.mailList;

    } catch (e) {
      this.mails = [];
    }
  }
  navigate(mail) {
    this.mailService.setMailToRead(mail);
  }
}
