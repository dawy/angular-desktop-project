import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Mail} from '../../../../Model';
import {MailServiceService} from '../../../../core/services';

@Component({
  selector: 'app-mail-item',
  templateUrl: './mail-item.component.html',
  styleUrls: ['./mail-item.component.scss']
})
export class MailItemComponent implements OnInit {
  @Input() mail: Mail;
  imgName: string;
  checked: boolean;
  constructor(private router: Router, private mailService: MailServiceService) { }

  ngOnInit() {
    try {
      const helper: string[] = this.mail.images[0].name.split('\\');
      this.imgName = helper[ helper.length - 1];
    } catch (e) {
      this.imgName = '';
    }
  }

  showMail() {
    this.router.navigateByUrl('/home');
  }
  handleChecked() {
    this.checked = !this.checked;
    if (this.checked === true) {
      this.mailService.addMailToDelete(this.mail);
    } else {
      this.mailService.removeMailToDelete(this.mail);
    }
  }
}
