import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {SettingsService} from '../../../../core/services';

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.scss']
})
export class BarComponent implements OnInit {

  constructor(private router: Router, public settingsService: SettingsService) {
  }

  ngOnInit() {
  }

  search() {
    this.router.navigateByUrl('search/result');
  }
}

