import { Component, Input, OnInit } from '@angular/core';
import {Result} from '../../../../Model/result';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  @Input() result: Result = new Result();
  constructor() { }

  ngOnInit() {
  }

}
