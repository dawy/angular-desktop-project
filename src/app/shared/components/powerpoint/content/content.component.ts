import { Component, OnInit } from '@angular/core';
import {SettingsService} from '../../../../core/services';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  image: {};
  constructor(private settingsService: SettingsService) { }

  ngOnInit() {
  }
}
