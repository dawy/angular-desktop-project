import { HostListener, Component, OnInit } from '@angular/core';
import {SettingsService} from '../../../../core/services';

export enum KEY {
  RIGHT_ARROW = 'ArrowRight',
  LEFT_ARROW = 'ArrowLeft',
  SPACEBAR = ' '
}

@Component({
  selector: 'app-left-bar',
  templateUrl: './left-bar.component.html',
  styleUrls: ['./left-bar.component.scss']
})
export class LeftBarComponent implements OnInit {
  selectedImage: number;
  presentationInterval: any;
  presentationPlaying: boolean;
  images = [
    {
      src: 'assets/presentations/1.png'
    },
    {
      src: 'assets/presentations/2.png'
    },
    {
      src: 'assets/presentations/3.png'
    },
    {
      src: 'assets/presentations/4.png'
    },
    {
      src: 'assets/presentations/5.png'
    },
    {
      src: 'assets/presentations/6.png'
    },
    {
      src: 'assets/presentations/7.png'
    },
    {
      src: 'assets/presentations/8.png'
    },
    {
      src: 'assets/presentations/9.png'
    },
    {
      src: 'assets/presentations/10.png'
    }
    ];
  icons = [
    {
      src: 'assets/presentations/svg/icon-left.svg'
    },
    {
      src: 'assets/presentations/svg/icon-right.svg'
    },
    {
      src: 'assets/presentations/svg/icon-move.svg'
    }
  ];
  constructor(private settingsService: SettingsService) { }

  ngOnInit() {
    this.selectedImage = this.images.length;
    this.presentationPlaying = false;
  }
  showImage(i: number) {
    this.selectedImage = i;
    this.settingsService.setPowerPointImage(this.images[i]);
  }
  onLeft() {
    console.log(event);
    this.selectedImage === 0 ? this.selectedImage = this.images.length - 1 : this.selectedImage--;
    this.showImage(this.selectedImage);
  }
  onRight() {
    console.log(event);
    this.selectedImage >= this.images.length - 1 ? this.selectedImage = 0 : this.selectedImage++;
    this.showImage(this.selectedImage);
  }
  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    event.preventDefault();

    if (event.key === KEY.RIGHT_ARROW) {
      this.onRight();
    }

    if (event.key === KEY.LEFT_ARROW) {
      this.onLeft();
    }

    if (event.key === KEY.SPACEBAR) {
      this.presentationPlaying = !this.presentationPlaying;
      if (!this.presentationPlaying) {
        clearInterval(this.presentationInterval);
      } else {
        this.presentationInterval = setInterval(() => {
          this.onRight();
        }, 5000);
      }
    }
  }
}
