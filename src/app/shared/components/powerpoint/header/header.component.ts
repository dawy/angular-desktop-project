import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  menuItems = [
    {
      src: 'assets/presentations/svg/icon-add.svg',
      text: 'Einfügen'
    },
    {
      src: 'assets/presentations/svg/icon-disketa.svg',
      text: 'Datei'
    },
    {
      src: 'assets/presentations/svg/icon-folders.svg',
      text: 'Bearbeiten'
    },
    {
      src: 'assets/presentations/svg/icon-wrench.svg',
      text: 'Organisieren'
    },
    {
      src: 'assets/presentations/svg/icon-cloud-upload.svg',
      text: 'Ansehen'
    },
    {
      src: 'assets/presentations/svg/icon-images.svg',
      text: 'Zubehör'
    },
    {
      src: 'assets/presentations/svg/icon-pie.svg',
      text: 'Diagramme'
    }
    ];
  constructor(private router: Router) { }

  ngOnInit() {
  }
  goHome() {
    this.router.navigateByUrl('');
  }
}
