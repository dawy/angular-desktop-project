import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {DesktopComponent} from './desktop.component';

const routes: Routes = [
  {
    path: '',
    component: DesktopComponent
  }
];

@NgModule({
  declarations: [DesktopComponent],
  imports: [CommonModule, SharedModule, FormsModule, RouterModule.forChild(routes)]
})
export class DesktopModule {}
