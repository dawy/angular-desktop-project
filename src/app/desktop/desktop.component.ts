import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-desktop',
  templateUrl: './desktop.component.html',
  styleUrls: ['./desktop.component.scss']
})
export class DesktopComponent implements OnInit {
  folderNames: Array<string> = [
     'Lacombe 082457 ',
     'Urs Federer 205938 ',
     'Y. Woah 708173 ',
     'Delikt: Hinks 053032 ',
     'Pascal Henry 654897 ',
     'Fall: Bahnhof 497690 ',
     'Panata 1507642 ',
     'Delikt: Heinz Hermann 364421 ',
     'Fall: Provvedi 1098346 ',
     'Michael Pont 0535543 ',
     'Frei 659310 ',
     'Gökhan 8090432 ',
     'Fiala 3500153 ',
     'Granit Zuber 2016100 ',
     'Denis Embolo 096547 ',
     'Mario Drmic 321654 ',
     'Blerim Shaqiri 255778 ',
     'Johannes Schär 030401 ',
     'Behrami 6500987 ',
     'Fall: Fernandes 020206 ',
     'Delikt: Seferovic 1517290 ',
     'Dzemaili 4076409 ',
     'Ryshavy 100297 '];
  folderWindowShown: boolean;
  audioWindowShown: boolean;
  time: number;
  duration: number;
  running: boolean;
  counterInterval: any;

  ngOnInit(): void {
    this.folderWindowShown = false;
    this.running = false;
    this.time = 0;
    this.duration = 28;
  }

  toggleFolderWindow() {
    this.folderWindowShown = !this.folderWindowShown;
  }

  toggleAudioWindow() {
    this.audioWindowShown = !this.audioWindowShown;
    this.resetAudio();
    this.toggleAudio();
  }

  getFormattedTime() {
    if (this.time < 10) {
      return '0' + this.time;
    } else {
      return this.time;
    }
  }

  toggleAudio() {
    if (!this.running) {
      this.counterInterval = setInterval(() => {
        if (this.time === this.duration) {
          this.time = 0;
        }
        this.time++;
      }, 1000);
      this.running = true;
    } else {
      clearInterval(this.counterInterval);
      this.running = false;
    }
  }

  resetAudio() {
    this.running = true;
    this.toggleAudio();
    this.time = 0;
  }

  addTime() {
    this.duration++;
  }

  removeTime() {
    this.duration--;
  }
}
